package display 
{
	import flash.display3D.textures.Texture;
	/**
	 * ...
	 * @author mawo
	 */
	public class MovieClipChild extends Bitmap3D
	{
		private var frame:int;
		private var keyframe:Keyframe;
		private var timeline:Timeline;
		public function MovieClipChild(texture:Texture3D, timeline:Timeline) 
		{
			super(texture);
			this.timeline = timeline;
			this.keyframe = timeline.keyframes[0];
			updateProperties(keyframe.x, keyframe.y, keyframe.scaleX, keyframe.scaleY, keyframe.alpha);
			frame = 1;
		}
		override public function render(renderer:Renderer):void
		{
			frame++;
			if (frame > timeline.totalFrames)
			{
				frame = 1;
				this.keyframe = timeline.keyframes[0];
				updateProperties(keyframe.x, keyframe.y, keyframe.scaleX, keyframe.scaleY, keyframe.alpha);
			} 
			else if (frame == keyframe.next.index)
			{
				keyframe = keyframe.next;
				updateProperties(keyframe.x, keyframe.y, keyframe.scaleX, keyframe.scaleY, keyframe.alpha);
			}
			else
			{
				updateProperties(x + keyframe.dx, y + keyframe.dy, scaleX + keyframe.dScaleX, scaleY + keyframe.dScaleY, alpha + keyframe.dAlpha);
			}
			renderer.render(this);
		}
		private function updateProperties(x:Number, y:Number, scaleX:Number, scaleY:Number, alpha:Number):void
		{
			this.x = x;
			this.y = y;
			this.scaleX = scaleX;
			this.scaleY = scaleY;
			this.alpha = alpha;
		}
	}

}