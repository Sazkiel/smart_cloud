package display
{
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author mawo
	 */
	public class Layer extends DisplayObjectContainer3D
	{
		public function Layer() 
		{
			mouseEnabled = true;
		}
		override public function getGlobalX():Number
		{
			return 0;
		}		
		override public function getGlobalY():Number
		{
			return 0;
		}		
		override public function getGlobalScaleX():Number
		{
			return 1;
		}		
		override public function getGlobalScaleY():Number
		{
			return 1;
		}
	}

}