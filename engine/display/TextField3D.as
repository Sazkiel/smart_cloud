package display
{
	/**
	 * ...
	 * @author mawo
	 */
	public class TextField3D extends DisplayObjectContainer3D
	{
		private var font:Font;
		private var m_text:String;
		public function TextField3D(font:Font) 
		{
			this.font = font;
		}
		public function set text(value:String):void
		{
			m_text = value
			removeChildren();
			for (var i:int = 0; i < value.length; i++)
			{
				if (value.charAt(i) == " ")
					continue;
				var letter:Bitmap3D = font.getLetter(value.charAt(i));
				setLetterPosition(letter);
				addChild(letter);
			}
		}
		
		private function setLetterPosition(letter:Bitmap3D):void 
		{
			var x:Number = 0;
			for each(var child:DisplayObject3D in children)
			{
				x += child.width;
			}
			letter.x = x;
			letter.y = 0;
		}
		public function get text():String
		{
			return m_text;
		}
	}

}