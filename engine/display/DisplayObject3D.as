package display
{
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author mawo
	 */
	public class DisplayObject3D extends EventDispatcher
	{
		public var texture:Texture3D;
		public var parent:DisplayObjectContainer3D;
		public var x:Number;
		public var y:Number;
		public var offsetX:Number;
		public var offsetY:Number;
		public var scaleX:Number;
		public var scaleY:Number;
		public var alpha:Number;
		public var mouseEnabled:Boolean;
		public var blendType:String;
		public function DisplayObject3D(texture:Texture3D = null) 
		{
			this.texture = texture;
			x = 0;
			y = 0;
			offsetX = 0;
			offsetY = 0;
			scaleX = 1;
			scaleY = 1;
			alpha = 1;
			blendType = BlendType.ALPHA;
		}
		public function set width(value:Number):void
		{
			scaleX = value / texture.frame.width;
		}		
		public function get width():Number
		{
			return scaleX * texture.frame.width;
		}		
		public function set height(value:Number):void
		{
			scaleY = value / texture.frame.height;
		}		
		public function get height():Number
		{
			return scaleY * texture.frame.height;
		}
		public function getGlobalX():Number
		{
			return parent.getGlobalX() + (x + offsetX) * parent.getGlobalScaleX();
		}		
		public function getGlobalY():Number
		{
			return parent.getGlobalY() + (y + offsetY) * parent.getGlobalScaleY();
		}		
		public function getGlobalScaleX():Number
		{
			return parent.getGlobalScaleX() * scaleX;
		}		
		public function getGlobalScaleY():Number
		{
			return parent.getGlobalScaleY() * scaleY;
		}
		public function render(renderer:Renderer):void
		{
			renderer.render(this);
		}
		public function hitTest(e:MouseEvent):Boolean 
		{
			var passX:Boolean;
			var passY:Boolean;
			if (mouseEnabled)
			{
				passX = getGlobalX() < e.stageX && (getGlobalX() + texture.frame.width * getGlobalScaleX()) > e.stageX;
				passY = getGlobalY() < e.stageY && (getGlobalY() + texture.frame.height * getGlobalScaleY()) > e.stageY;
				if (passX && passY)
					dispatchEvent(e);
			}
			return passX && passY;
		}
		
	}

}