package display
{
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author mawo
	 */
	public class DisplayObjectContainer3D extends DisplayObject3D
	{
		protected var children:Vector.<DisplayObject3D>;
		protected var m_width:Number;
		protected var m_height:Number;
		public function DisplayObjectContainer3D() 
		{
			children = new Vector.<DisplayObject3D>();
		}
		public function addChild(child:DisplayObject3D):DisplayObject3D
		{
			if (child && child.parent)
				child.parent.removeChild(child);
			children.push(child);
			child.parent = this;
			updateProperties();
			return child;
		}
		public function removeChild(child:DisplayObject3D):DisplayObject3D
		{
			var index:int = children.indexOf(child);
			if (index >= 0)
			{
				children.splice(index, 1);
				child.parent = null;
			}
			return child;
		}
		public function removeChildren():void
		{
			while (children.length)
				removeChild(children[0]);
		}
		protected function updateProperties():void 
		{
			var left:Number = 0;
			var right:Number = 0;
			var top:Number = 0;
			var bottom:Number = 0;
			for (var i:int; i < children.length; i++)
			{
				if (children[i].x < left)
					left = children[i].x;
				if (children[i].x + children[i].width > right)
					right = children[i].x + children[i].width;
				if (children[i].y < top)
					top = children[i].y;
				if (children[i].y + children[i].height > bottom)
					bottom = children[i].y + children[i].height;
			}
			m_width = right - left;
			m_height = bottom - top;
		}
		override public function set width(value:Number):void
		{
			scaleX = value / m_width;
		}		
		override public function get width():Number
		{
			return scaleX * m_width;
		}		
		override public function set height(value:Number):void
		{
			scaleY = value / m_height;
		}		
		override public function get height():Number
		{
			return scaleY * m_height;
		}
		override public function render(renderer:Renderer):void
		{
			for (var i:int; i < children.length; i++)
			{
				children[i].render(renderer);
			}
		}
		override public function hitTest(e:MouseEvent):Boolean 
		{
			if (mouseEnabled)
			{
				for (var i:int = children.length - 1; i >= 0; i--)
				{
					if (children[i].hitTest(e))
					{
						dispatchEvent(e);
						return true;
					}
				}
			}
			return false;
		}
	}

}