package  
{
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	/**
	 * ...
	 * @author mawo
	 */
	public class AnimationLoader extends EventDispatcher
	{
		public var data:Object;
		public var loaded:Boolean
		public function AnimationLoader(name:String) 
		{
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, onComplete);
			loader.load(new URLRequest("animations/" + name + ".txt"));
		}
		
		private function onComplete(e:Event):void 
		{
			var loader:URLLoader = e.target as URLLoader;
			loader.removeEventListener(Event.COMPLETE, onComplete);
			data = JSON.parse(loader.data);
			loaded = true;
			dispatchEvent(new Event(EngineEvents.ANIMATION_LOADED));
		}
		
	}

}