package tween
{
	import display.DisplayObject3D;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.utils.getTimer;
	/**
	 * ...
	 * @author mawo
	 */
	public class Tweener extends Sprite
	{
		private static var instance:Tweener;
		{
			instance = new Tweener();
			tweens = new Vector.<BasicTween>();
		}
		private static var tweens:Vector.<BasicTween>;
		private static var running:Boolean;
		private static var lastTime:Number;
		public function Tweener() 
		{
			
		}
		public static function move(target:DisplayObject3D, x:Number, y:Number, time:Number /*ms*/, type:String = "linear"):void
		{
			var m:Move = new Move(target, x, y, time, type);
			tweens.push(m);
			start();
		}
		public static function call(fun:Function, delay:Number /*ms*/, parameters:Array = null):void
		{
			var c:Call = new Call(fun, delay, parameters);
			tweens.push(c);
			start();
		}
		private static function start():void 
		{
			if (running == false)
			{
				instance.addEventListener(Event.ENTER_FRAME, onEnterFrame);
				lastTime = getTimer();
				running = true;
			}
		}
		private static function stop():void
		{
			instance.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			running = false;
		}
		private static function onEnterFrame(e:Event):void 
		{
			var now:Number = getTimer();
			var dt:Number = now - lastTime;
			lastTime = now;
			for (var i:int = tweens.length - 1; i >= 0; i--)
			{
				if (tweens[i].process(dt) == false)
				{
					tweens.splice(i, 1);
				}
			}
			
			if (tweens.length == 0)
			{
				stop();
			}
		}
	}

}