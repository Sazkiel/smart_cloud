package tween 
{
	/**
	 * ...
	 * @author mawo
	 */
	public class Call extends BasicTween
	{
		private var fun:Function;
		private var delay:Number;
		private var parameters:Array;
		public function Call(fun:Function, delay:Number, parameters:Array) 
		{
			this.fun = fun;
			this.delay = delay;
			this.parameters = parameters;
		}
		
		override public function process(dt:Number):Boolean
		{
			if (delay > 0)
			{
				delay -= dt;
				return true;
			}
			else
			{
				fun.apply(null, parameters);
				return false;
			}
		}
	}

}