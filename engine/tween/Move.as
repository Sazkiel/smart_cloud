package tween
{
	import display.DisplayObject3D;
	/**
	 * ...
	 * @author mawo
	 */
	public class Move extends BasicTween
	{
		private var target:DisplayObject3D;
		private var typeFunction:Function;
		private var time:Number;
		private var timeLeft:Number;
		private var params:Object;
		public function Move(target:DisplayObject3D, x:Number, y:Number, time:Number, type:String) 
		{
			
			switch(type)
			{
				case MoveType.LINEAR:
					params = { speedX:(x - target.x) / time, speedY:(y - target.y) / time };
					typeFunction = linear;
					break;				
				case MoveType.BEZIER:
					params = { ax:target.x, ay:target.y, bx:100, by:200, cx:100, cy:50, dx:x, dy:y };
					typeFunction = bezier;
					break;
			}
			this.target = target;
			this.time = time;
			this.timeLeft = time;
		}
		override public function process(dt:Number):Boolean
		{
			if (timeLeft > 0)
			{
				timeLeft -= dt;
				typeFunction(dt);
				return true;
			}
			else
			{
				return false;
			}
			
			
		}
		private function linear(dt:Number):void
		{
			target.x += params.speedX * dt;
			target.y += params.speedY * dt;
		}
		private function bezier(dt:Number):void
		{
			var t:Number = timeLeft / time;
			target.x = params.ax * Math.pow(1 - t, 3) + 3 * params.bx * t * Math.pow(1 - t, 2) + 3 * params.cx * Math.pow(t, 2) * (1 - t) + params.dx * Math.pow(t, 3);
			target.y = params.ay * Math.pow(1 - t, 3) + 3 * params.by * t * Math.pow(1 - t, 2) + 3 * params.cy * Math.pow(t, 2) * (1 - t) + params.dy * Math.pow(t, 3);
		}
	}

}