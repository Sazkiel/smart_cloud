package managers
{
	import flash.display.Bitmap;
	import flash.display3D.textures.Texture;
	import flash.display3D.VertexBuffer3D;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author mawo
	 */
	public class TextureManager extends EventDispatcher
	{
		private var renderer:Renderer;
		private var textures:Dictionary;
		private var loaders:Vector.<TextureLoader>;
		public function TextureManager(renderer:Renderer) 
		{
			this.renderer = renderer;
			this.textures = new Dictionary();
			this.loaders = new Vector.<TextureLoader>();
		}
		public function loadTextures(textures:Vector.<String>):void 
		{
			
			for each(var texture:String in textures)
			{
				var loader:TextureLoader = new TextureLoader(texture);
				loader.addEventListener(EngineEvents.TEXTURE_LOADED, onTextureLoaded);
				loaders.push(loader);
			}
		}
		private function onTextureLoaded(e:Event):void 
		{
			var json:Object = e.target.getJson();
			var texture:Texture = renderer.getTexture(json.meta.size.w, json.meta.size.h);
			texture.uploadCompressedTextureFromByteArray(e.target.getAtf(), 0, true);
			for each(var asset:Object in json.frames)
			{
				var name:String = asset.filename.split(".")[0];
				var frame:Frame = new Frame(asset, json);
				var vertexBuffer:VertexBuffer3D = renderer.getVertexBuffer3D(4, 11);
				var texture3D:Texture3D = new Texture3D(texture, frame, vertexBuffer);
				textures[name] = texture3D;
			}
			if(complete)
				dispatchEvent(new Event(Event.COMPLETE));
		}
		public function getTexture3D(name:String):Texture3D
		{
			return textures[name];
		}
		public function get complete():Boolean
		{
			for each(var loader:TextureLoader in loaders)
				if (loader.loaded == false)
					return false;
			return true;
		}
	}

}