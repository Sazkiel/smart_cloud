package managers
{
	import display.DisplayObject3D;
	import display.Layer;
	/**
	 * ...
	 * @author mawo
	 */
	public class LayerManager 
	{
		public var layers:Vector.<Layer>;
		public function LayerManager(layerCount:int) 
		{
			layers = new Vector.<Layer>(layerCount);
			for (var i:int = 0; i < layerCount; i++)
			{
				layers[i] = new Layer();
			}
		}
		public function get layerCount():int
		{
			return layers.length;
		}
		public function addToLayer(object:DisplayObject3D, layer:int):void
		{
			layers[layer].addChild(object);
		}
		
		public function render(renderer:Renderer):void 
		{
			for (var i:int = 0; i < layers.length; i++)
			{
				layers[i].render(renderer);
			}
		}
	}

}