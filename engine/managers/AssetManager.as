package managers
{
	import display.Bitmap3D;
	import display.MovieClip3D;
	import display.MovieClipChild;
	import flash.display3D.textures.Texture;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	/**
	 * ...
	 * @author mawo
	 */
	public class AssetManager extends EventDispatcher
	{
		private var textureManager:TextureManager;
		private var animationManager:AnimationManager;
		public function AssetManager(textureManager:TextureManager, animationManager:AnimationManager) 
		{
			this.textureManager = textureManager;
			this.animationManager = animationManager;
		}
		public function getBitmap3D(name:String):Bitmap3D
		{
			var texture:Texture3D = textureManager.getTexture3D(name);
			return new Bitmap3D(texture);
		}
		public function getMovieClip3D(name:String):MovieClip3D
		{
			var animation:Animation = animationManager.getAnimation(name);
			var movie:MovieClip3D = new MovieClip3D();
			for (var i:int = 0; i < animation.timelines.length; i++)
			{
				var timeline:Timeline = animation.timelines[i];
				var texture:Texture3D = textureManager.getTexture3D(timeline.name);
				var child:MovieClipChild = new MovieClipChild(texture, timeline);
				movie.addChild(child);
			}
			return movie;
		}
	}

}