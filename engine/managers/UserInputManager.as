package managers
{
	import flash.display.Stage;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author mawo
	 */
	public class UserInputManager extends EventDispatcher
	{
		private var layerManager:LayerManager;
		private var stage:Stage;
		public function UserInputManager(stage:Stage, layerManager:LayerManager) 
		{
			this.stage = stage;
			this.layerManager = layerManager;
			
			stage.addEventListener(MouseEvent.CLICK, onMouseEvent);
			stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseEvent);
			stage.addEventListener(MouseEvent.MOUSE_UP, onMouseEvent);
			stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseEvent);
		}
		
		private function onMouseEvent(e:MouseEvent):void 
		{
			for (var i:int = layerManager.layerCount - 1; i >= 0; i--)
			{
				if (layerManager.layers[i].hitTest(e))
					break;
			}
			dispatchEvent(e);
		}
		
	}

}