package managers
{
	import display.TextField3D;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.utils.Dictionary;
	/**
	 * ...
	 * @author mawo
	 */
	public class FontManager extends EventDispatcher
	{
		private var fonts:Dictionary;
		private var textureManager:TextureManager;
		public function FontManager(textureManager:TextureManager) 
		{
			fonts = new Dictionary();
			this.textureManager = textureManager;
		}

		public function loadFonts(styles:Vector.<String>):void
		{
			for each(var style:String in styles)
			{
				fonts[style] = new Font(textureManager, style);
				fonts[style].addEventListener(Event.COMPLETE, onComplete);
			}
		}
		
		private function onComplete(e:Event):void 
		{
			e.target.removeEventListener(Event.COMPLETE, onComplete);
			for each(var font:Font in fonts)
				if (font.loaded == false)
					return;
			if(complete)
				dispatchEvent(new Event(Event.COMPLETE));
		}
		public function getTextField3D(style:String):TextField3D
		{
			return new TextField3D(fonts[style]);
		}
		public function get complete():Boolean
		{
			for each(var font:Font in fonts)
			{
				if (font.loaded == false)
					return false;
			}
			return true;
		}
	}

}