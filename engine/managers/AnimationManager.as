package managers
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.utils.Dictionary;
	
	/**
	 * ...
	 * @author mawo
	 */
	public class AnimationManager extends EventDispatcher
	{
		private var animations:Dictionary;
		private var loaders:Vector.<AnimationLoader>;
		public function AnimationManager()
		{
			this.animations = new Dictionary();
			loaders = new Vector.<AnimationLoader>();
		}
		public function loadAnimations(animations:Vector.<String>):void
		{
			for each (var animation:String in animations)
			{
				var loader:AnimationLoader = new AnimationLoader(animation);
				loader.addEventListener(EngineEvents.ANIMATION_LOADED, onAnimationLoaded);
				loaders.push(loader);
			}
		}
		private function onAnimationLoaded(e:Event):void
		{
			var loader:AnimationLoader = e.target as AnimationLoader;
			loader.removeEventListener(EngineEvents.ANIMATION_LOADED, onAnimationLoaded);
			var data:Object = loader.data;
			for each(var anim:Object in data.animations)
			{
				var animation:Animation = new Animation(anim.name);
				for each(var timeline:Object in anim.timelines)
				{
					animation.addTimeline(timeline);
				}
				animations[animation.name] = animation;
			}
			if (complete)
				dispatchEvent(new Event(Event.COMPLETE));
		}
		public function getAnimation(name:String):Animation
		{
			return animations[name];
		}
		public function get complete():Boolean
		{
			for each(var loader:AnimationLoader in loaders)
				if (loader.loaded == false)
					return false;
			return true;
		}
	}

}