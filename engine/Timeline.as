package  
{
	/**
	 * ...
	 * @author mawo
	 */
	public class Timeline 
	{
		public var totalFrames:int;
		public var name:String;
		public var keyframes:Vector.<Keyframe>;
		public function Timeline(name:String) 
		{
			this.name = name;
			this.keyframes = new Vector.<Keyframe>();
		}
		public function addKeyframe(source:Object):void
		{
			var keyframe:Keyframe = new Keyframe();
			keyframe.index = source.index;
			keyframe.x = source.x;
			keyframe.y = source.y;
			keyframe.scaleX = source.scaleX;
			keyframe.scaleY = source.scaleY;
			keyframe.alpha = source.alpha;
			keyframes.push(keyframe);
		}
		public function setup():void
		{
			for (var i:int = 0; i < keyframes.length; i++)
			{
				var j:int = i + 1;
				if (j == keyframes.length)
					j = 0;
				var frameDiff:Number = keyframes[j].index - keyframes[i].index;
				keyframes[i].dx = (keyframes[j].x - keyframes[i].x) / frameDiff;
				keyframes[i].dy = (keyframes[j].y - keyframes[i].y) / frameDiff;
				keyframes[i].dScaleX = (keyframes[j].scaleX - keyframes[i].scaleX) / frameDiff;
				keyframes[i].dScaleY = (keyframes[j].scaleY - keyframes[i].scaleY) / frameDiff;
				keyframes[i].dAlpha = (keyframes[j].alpha - keyframes[i].alpha) / frameDiff / 100;
				keyframes[i].next = keyframes[j];
			}
			totalFrames = keyframes[keyframes.length - 1].index + 1;
		}
	}

}