package  
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	/**
	 * ...
	 * @author mawo
	 */
	public class TextureLoader extends EventDispatcher
	{
		public var loaded:Boolean;
		private var name:String;
		private var atf:URLLoader;
		private var json:URLLoader;
		public function TextureLoader(name:String) 
		{
			this.name = name;
			loadTexture();
		}
		private function loadTexture():void 
		{
			atf = new URLLoader();
			atf.dataFormat = URLLoaderDataFormat.BINARY;
			atf.addEventListener(Event.COMPLETE, onAtfComplete);
			atf.load(new URLRequest("atf/" + name + ".atf"));
		}
		private function onAtfComplete(e:Event):void 
		{
			json = new URLLoader();
			json.dataFormat = URLLoaderDataFormat.TEXT;
			json.addEventListener(Event.COMPLETE, onJsonComplete);
			json.load(new URLRequest("json/" + name + ".json"));
		}
		public function getJson():Object
		{
			return JSON.parse(json.data);
		}
		public function getAtf():ByteArray
		{
			return atf.data;
		}
		private function onJsonComplete(e:Event):void 
		{
			loaded = true;
			dispatchEvent(new Event(EngineEvents.TEXTURE_LOADED));
		}
		
	}

}