package  
{
	/**
	 * ...
	 * @author mawo
	 */
	public class Frame 
	{
		public var x:int;
		public var y:int;
		public var width:int;
		public var height:int;
		public var rotated:Boolean;
		public var textureWidth:int;
		public var textureHeight:int;
		public var sourceWidth:int;
		public var sourceHeight:int;
		public var sourceX:int;
		public var sourceY:int;
		public function Frame(asset:Object, json:Object) 
		{
			this.x = asset.frame.x;
			this.y = asset.frame.y;
			this.width = asset.frame.w;
			this.height = asset.frame.h;
			this.rotated = asset.rotated;
			this.textureWidth = json.meta.size.w;
			this.textureHeight = json.meta.size.h;
			this.sourceWidth = asset.sourceSize.w;
			this.sourceHeight = asset.sourceSize.h;
			this.sourceX = asset.spriteSourceSize.x;
			this.sourceY = asset.spriteSourceSize.y;
		}
		
	}

}