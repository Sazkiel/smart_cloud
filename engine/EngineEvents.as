package  
{
	/**
	 * ...
	 * @author mawo
	 */
	public class EngineEvents 
	{
		static public const ENGINE_CREATED:String = "engineCreated";
		static public const TEXTURE_LOADED:String = "textureLoaded";
		static public const ANIMATION_LOADED:String = "animationLoaded";
		static public const READY:String = "ready";
	}

}