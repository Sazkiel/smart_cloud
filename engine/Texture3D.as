package  
{
	import flash.display3D.Context3D;
	import flash.display3D.textures.Texture;
	import flash.display3D.VertexBuffer3D;
	/**
	 * ...
	 * @author mawo
	 */
	public class Texture3D 
	{
		public var frame:Frame;
		public var texture:Texture;
		private var meshVertexData:Vector.<Number>;
		public var vertexBuffer:VertexBuffer3D;
		public var uv:Vector.<Number>;
		public function Texture3D(texture:Texture, frame:Frame, vertexBuffer:VertexBuffer3D) 
		{
			this.texture = texture;
			this.frame = frame;
			setupVertexData();
			setupUV();
			vertexBuffer.uploadFromVector(meshVertexData, 0, meshVertexData.length / 11);
			this.vertexBuffer = vertexBuffer;
		}
		private function setupUV():void
		{
			uv = new Vector.<Number>(8);
			var width:Number;
			var height:Number;
			if (frame.rotated) {
				width = frame.height;
				height = frame.width;
			}
			else
			{
				width = frame.width;
				height = frame.height;
			}
			
			
            uv[0] = frame.x / frame.textureWidth;
            uv[1] = frame.y / frame.textureHeight;
			
            uv[2] = (frame.x + width) / frame.textureWidth;
            uv[3] = frame.y / frame.textureHeight;
			
            uv[4] = (frame.x + width) / frame.textureWidth;
            uv[5] = (frame.y + height) / frame.textureHeight;
			
            uv[6] = frame.x / frame.textureWidth;
            uv[7] = (frame.y + height) / frame.textureHeight;             
			
			if (frame.rotated)
			{
				uv.push(uv.shift());
				uv.push(uv.shift());
			}
		}
		private function setupVertexData():void
		{
			meshVertexData = Vector.<Number> 
			( [
				//X,	Y,   Z,    	UV mask used in shader		
				-0.5, -0.5,  0, 	1, 1, 0, 0, 0, 0, 0, 0,
				 0.5, -0.5,  0,		0, 0, 1, 1, 0, 0, 0, 0,
				 0.5,  0.5,  0,		0, 0, 0, 0, 1, 1, 0, 0,
				-0.5,  0.5,  0,		0, 0, 0, 0, 0, 0, 1, 1,
			]);
			
			// We want the same vertex data as source rect
			//meshVertexData[0] = owner.visibleArea.left / owner.visibleArea.width;
			//meshVertexData[1] = owner.visibleArea.top / owner.visibleArea.height;
			//
			//meshVertexData[11] = owner.visibleArea.right / owner.visibleArea.width;
			//meshVertexData[12] = owner.visibleArea.top / owner.visibleArea.height;
			//
			//meshVertexData[22] = owner.visibleArea.right / owner.visibleArea.width;
			//meshVertexData[23] = owner.visibleArea.bottom / owner.visibleArea.height;
			//
			//meshVertexData[33] = owner.visibleArea.left / owner.visibleArea.width;
			//meshVertexData[34] = owner.visibleArea.bottom / owner.visibleArea.height;					
			
			
			meshVertexData[0] = 0;
			meshVertexData[1] = 0;
			
			meshVertexData[11] = frame.width;
			meshVertexData[12] = 0;
			
			meshVertexData[22] = frame.width;
			meshVertexData[23] = frame.height;
			
			meshVertexData[33] = 0;
			meshVertexData[34] = frame.height;			
		}
	}

}