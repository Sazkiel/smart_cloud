﻿package
{
	import com.adobe.utils.*;
	import display.DisplayObject3D;
	import flash.display.*;
	import flash.display3D.*;
	import flash.display3D.textures.*;
	import flash.events.*;
	import flash.geom.*;
	import flash.utils.*;
	
	public class Renderer extends EventDispatcher
	{
		// the 3d graphics window on the stage
		public var context3D:Context3D;
		public var complete:Boolean;
		// the compiled shader used to render our mesh
		private var shaderProgram:Program3D;
		// the uploaded verteces used by our mesh
		private var vertexBuffer:VertexBuffer3D;
		// the uploaded indeces of each vertex of the mesh
		private var indexBuffer:IndexBuffer3D;
		// the data that defines our 3d mesh model
		private var meshVertexData:Vector.<Number>;
		// the indeces that define what data is used by each vertex
		private var meshIndexData:Vector.<uint>;
		
		// matrices that affect the mesh location and camera angles
		private var projectionMatrix:Matrix3D = new Matrix3D();
		private var modelMatrix:Matrix3D = new Matrix3D();
		private var viewMatrix:Matrix3D = new Matrix3D();
		private var modelViewProjection:Matrix3D = new Matrix3D();
		private var viewProjMatrix:Matrix3D = new Matrix3D();
		// a simple frame counter used for animation
		private var t:Number = 0;
		private var blendType:String;
		// The Stage3d Texture that uses the above myTextureData
		private var myTexture:Texture;

		private var stage:Stage;
		
		public function Renderer(stage:Stage) 
		{
			this.stage = stage;
			init();
		}

		private function init():void 
		{
			// and request a context3D from Stage3d
			stage.stage3Ds[0].addEventListener(Event.CONTEXT3D_CREATE, onContext3DCreate);
			stage.stage3Ds[0].requestContext3D();
		}
		
		private function onContext3DCreate(event:Event):void 
		{	
			// Obtain the current context
			var t:Stage3D = event.target as Stage3D;					
			context3D = t.context3D; 	
			if (context3D == null) 
			{
				// Currently no 3d context is available (error!)
				return;
			}
			
			// Disabling error checking will drastically improve performance.
			// If set to true, Flash will send helpful error messages regarding
			// AGAL compilation errors, uninitialized program constants, etc.
			context3D.enableErrorChecking = true;
			context3D.setDepthTest(false, Context3DCompareMode.ALWAYS);
			// Defines which vertex is used for each polygon
			// In this example a square is made from two triangles
			meshIndexData = Vector.<uint> ([0, 1, 2,	0, 2, 3,]);
			
			// The 3d back buffer size is in pixels
			context3D.configureBackBuffer(stage.stageWidth, stage.stageHeight, 0, true);

			// A simple vertex shader which does a 3D transformation
			var vertexShaderAssembler:AGALMiniAssembler = new AGALMiniAssembler();
			vertexShaderAssembler.assemble
			( 
				Context3DProgramType.VERTEX,
				// 4x4 matrix multiply to get camera angle
				"m44 op, va0, vc0\n" +
				// tell fragment shader about XYZ
				"mov v0, va0\n" +
				// tell fragment shader about UV
				"mul vt0, va1, vc4\n" +
				"mul vt1, va2, vc5\n" +
				
				"add vt0.xy, vt0.xy, vt1.xy\n" +
				"add vt0.xy, vt0.xy, vt0.zw\n" +
				"add vt0.xy, vt0.xy, vt1.zw\n" +
				
				"mov v1, vt0\n"
			);			
			
			// A simple fragment shader which will use the vertex position as a color
			var fragmentShaderAssembler:AGALMiniAssembler = new AGALMiniAssembler();
			fragmentShaderAssembler.assemble
			( 
				Context3DProgramType.FRAGMENT,	
				// grab the texture color from texture fs0
				// using the UV coordinates stored in v1
				"tex oc, v1, fs0 <2d, norepeat, linear, nomip, dxt5>"
			);
			
			// combine shaders into a program which we then upload to the GPU
			shaderProgram = context3D.createProgram();
			shaderProgram.upload(vertexShaderAssembler.agalcode, fragmentShaderAssembler.agalcode);

			// upload the mesh indexes
			indexBuffer = context3D.createIndexBuffer(meshIndexData.length);
			indexBuffer.uploadFromVector(meshIndexData, 0, meshIndexData.length);
			
			// upload the mesh vertex data
			// since our particular data is 
			// x, y, z, u, v, nx, ny, nz
			// each vertex uses 8 array elements
			//vertexBuffer = context3D.createVertexBuffer(meshVertexData.length/8, 8); 
			//vertexBuffer.uploadFromVector(meshVertexData, 0, meshVertexData.length / 8);
			
			// create projection matrix for our 3D scene
			setupProjectionMatrix();
			complete = true;
			dispatchEvent(new Event(Event.COMPLETE));
		}
		public function getTexture(width:int, height:int):Texture
		{
			if (context3D)
				return context3D.createTexture(width, height, Context3DTextureFormat.COMPRESSED_ALPHA, true);
			else
				throw(new Error("There is no Context3D"));
		}		
		
		public function getVertexBuffer3D(numVertices:int, data32PerVertex:int):VertexBuffer3D
		{
			if (context3D)
				return context3D.createVertexBuffer(numVertices, data32PerVertex);
			else
				throw(new Error("There is no Context3D"));
		}
		public function setBlendType(value:String):void {
			if (blendType != value) {
				blendType = value;
				switch(blendType)
				{
					case BlendType.NONE:
						context3D.setBlendFactors(Context3DBlendFactor.ONE, Context3DBlendFactor.ZERO);
						break;
					case BlendType.ADDITIVE:
						context3D.setBlendFactors(Context3DBlendFactor.ONE, Context3DBlendFactor.ONE);
						break;
					case BlendType.MULTIPLY:
						context3D.setBlendFactors(Context3DBlendFactor.DESTINATION_COLOR, Context3DBlendFactor.ZERO);
						break;
					case BlendType.SCREEN:
						context3D.setBlendFactors(Context3DBlendFactor.ONE, Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA)
						break;					
					case BlendType.ALPHA:
						context3D.setBlendFactors(Context3DBlendFactor.SOURCE_ALPHA, Context3DBlendFactor.ONE_MINUS_SOURCE_ALPHA)
						break;
				}
			}
		}
		public function render(object:DisplayObject3D):void 
		{	
			if (object.alpha == 0)
				return;
			setBlendType(object.blendType);
			
			
			context3D.setProgram(shaderProgram);
			
			// create the various transformation matrices
			modelMatrix.identity();
			modelMatrix.appendRotation(0, Vector3D.Z_AXIS);
			modelMatrix.appendScale(object.getGlobalScaleX(), object.getGlobalScaleY(), 1);
			
			modelMatrix.appendTranslation(object.getGlobalX(), object.getGlobalY(), 0);
			//modelMatrix.appendTranslation(parentX + object.x, parentY + object.y, 0);
			
			// clear the matrix and append new angles
			modelViewProjection.identity();
			modelViewProjection.append(modelMatrix);
			modelViewProjection.append(viewMatrix);
			//modelViewProjection.append(projectionMatrix);
			
			// pass our matrix data to the shader program
			context3D.setProgramConstantsFromMatrix(Context3DProgramType.VERTEX, 0, modelViewProjection, true );
			context3D.setProgramConstantsFromVector(Context3DProgramType.VERTEX, 4, object.texture.uv);
			//context3D.setProgramConstantsFromByteArray(Context3DProgramType.VERTEX, 0, 2, data.uvb, 0);
			// associate the vertex data with current shader program
			// position
			context3D.setVertexBufferAt(0, object.texture.vertexBuffer, 0, Context3DVertexBufferFormat.FLOAT_3);
			
			// tex coord
			context3D.setVertexBufferAt(1, object.texture.vertexBuffer, 3, Context3DVertexBufferFormat.FLOAT_4);
			context3D.setVertexBufferAt(2, object.texture.vertexBuffer, 7, Context3DVertexBufferFormat.FLOAT_4);
			
			// which texture should we use?
			context3D.setTextureAt(0, object.texture.texture);
			
			// finally draw the triangles, two number is from meshIndexData.length/3
			context3D.drawTriangles(indexBuffer, 0, 2);
		}
		public function prepare():void
		{
			// clear scene before rendering is mandatory
			context3D.clear(0,0,0); 
		}
		public function present():void
		{
			// present/flip back buffer
			context3D.present();
		}
		public function onResize():void
		{
			if (context3D)
			{
				context3D.configureBackBuffer(stage.stageWidth, stage.stageHeight, 0, true);
				setupProjectionMatrix();
			}
		}
		private function setupProjectionMatrix():void
		{
			projectionMatrix = new Matrix3D(Vector.<Number>([
				2 / stage.stageWidth, 0, 0, 0,
				0, -2 / stage.stageHeight, 0, 0,
				0, 0, 0.01, 0,
				0, 0, 0, 1
			]));
				
			viewMatrix.identity();
			// We want have (0,0) point at top-left corner
			viewMatrix.appendTranslation( -stage.stageWidth / 2, -stage.stageHeight / 2, 0);
			viewMatrix.append(projectionMatrix);
		}
	}
}
