package  
{
	/**
	 * ...
	 * @author mawo
	 */
	public class BlendType 
	{
		public static const NONE:String = "NONE";
		public static const SCREEN:String = "SCREEN";
		public static const ALPHA:String = "ALPHA";
		public static const DEFAULT:String = "DEFAULT";		
		static public const ADDITIVE:String = "ADDITIVE";
		static public const MULTIPLY:String = "MULTIPLY";
	}

}