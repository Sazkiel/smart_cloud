package  
{
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import managers.AnimationManager;
	import managers.AssetManager;
	import managers.FontManager;
	import managers.LayerManager;
	import managers.TextureManager;
	import managers.UserInputManager;
	/**
	 * ...
	 * @author mawo
	 */
	public class Engine extends EventDispatcher
	{	
		public var stage:Stage;	
		private var layersCount:int;
		public var stageWidth:int;
		public var stageHeight:int;
		
		private var renderer:Renderer;
		public var assetManager:AssetManager;
		public var layerManager:LayerManager;
		public var userInputManager:UserInputManager;
		public var fontManager:FontManager;
		public var animationManager:AnimationManager;
		public var textureManager:TextureManager;
		public function Engine(stage:Stage) 
		{
			this.stage = stage;
			renderer = new Renderer(stage);
			renderer.addEventListener(Event.COMPLETE, onComplete);
			this.stage.frameRate = 60;
			this.layersCount = layersCount;
			stage.addEventListener(Event.RESIZE, onResize);
			onResize();
		}
		
		public function loadAnimations(animations:Vector.<String>):void 
		{
			animationManager = new AnimationManager();
			animationManager.addEventListener(Event.COMPLETE, onComplete);
			animationManager.loadAnimations(animations);
		}		
		public function loadTextures(textures:Vector.<String>):void 
		{
			textureManager = new TextureManager(renderer);
			textureManager.addEventListener(Event.COMPLETE, onComplete);
			textureManager.loadTextures(textures);
		}
		public function loadFonts(fonts:Vector.<String>):void
		{
			fontManager = new FontManager(textureManager);
			fontManager.addEventListener(Event.COMPLETE, onComplete);
			fontManager.loadFonts(fonts);
		}
		public function createLayers(count:int):void
		{
			layerManager = new LayerManager(count);
		}
		private function onResize(e:Event = null):void 
		{
			renderer.onResize();
			stageWidth = stage.stageWidth;
			stageHeight = stage.stageHeight;
		}
		
		private function onComplete(e:Event):void 
		{
			e.target.removeEventListener(Event.COMPLETE, onComplete);
			if (complete)
			{
				stage.addEventListener(Event.ENTER_FRAME, onEnterFrame);
				assetManager = new AssetManager(textureManager, animationManager);
				userInputManager = new UserInputManager(stage, layerManager);
				dispatchEvent(new Event(EngineEvents.ENGINE_CREATED));
			}
		}
		private function get complete():Boolean
		{
			if (renderer && renderer.complete == false)
				return false;
			if (textureManager && textureManager.complete == false)
				return false
			if (animationManager && animationManager.complete == false)
				return false				
			if (fontManager && fontManager.complete == false)
				return false		
				
			return true;
		}
		
		private function onEnterFrame(e:Event):void 
		{
			renderer.prepare();
			layerManager.render(renderer)
			renderer.present();
		}
	}

}