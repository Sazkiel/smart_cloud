package  
{
	/**
	 * ...
	 * @author mawo
	 */
	public class Keyframe 
	{
		public var next:Keyframe;
		public var index:Number;
		public var x:Number;
		public var y:Number;
		public var dx:Number;
		public var dy:Number;
		public var scaleX:Number;
		public var scaleY:Number;
		public var dScaleX:Number;
		public var dScaleY:Number;
		public var alpha:Number;
		public var dAlpha:Number;
		public function Keyframe() 
		{
			
		}
		
	}

}