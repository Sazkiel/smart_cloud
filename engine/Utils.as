package  
{
	import flash.system.Capabilities;
	/**
	 * ...
	 * @author me
	 */
	public class Utils 
	{
		
		public function Utils() 
		{
			
		}
		
		static public function nextTwoPower(value:int):int 
		{
			var result:int = 1;
			while (result < value) result *= 2;
			return result;
		}
		static public function os():String
		{
			var os:String = Capabilities.os;
			if (os.indexOf(OS.ANDROID) != -1)
				return OS.ANDROID;			
			if (os.indexOf(OS.IOS) != -1)
				return OS.IOS;
			return OS.PC;
		}
	}

}