package  
{
	/**
	 * ...
	 * @author mawo
	 */
	public class Animation 
	{
		public var name:String;
		public var timelines:Vector.<Timeline>;
		public function Animation(name:String) 
		{
			this.name = name;
			timelines = new Vector.<Timeline>();
		}
		
		public function addTimeline(source:Object):void 
		{
			var timeline:Timeline = new Timeline(source.name);
			for each(var keyFrame:Object in source.keyframes)
			{
				timeline.addKeyframe(keyFrame);
			}
			timeline.setup();
			timelines.push(timeline);
		}
		private function getTimeline(name:String):Timeline
		{
			var timeline:Timeline;
			for (var i:int = 0; i < timelines.length; i++)
			{
				if (timelines[i].name == name)
				{
					timeline = timelines[i];
					break;
				}
			}
			return timeline;
		}
	}

}