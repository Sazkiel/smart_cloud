package  
{
	import display.Bitmap3D;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.Dictionary;
	import managers.TextureManager;
	/**
	 * ...
	 * @author mawo
	 */
	public class Font extends EventDispatcher
	{
		private var offsets:Dictionary;
		public var loaded:Boolean;
		protected static var alphabet:Vector.<String> = Vector.<String>(["0", "1", "2", "3", "4", "5", "6", "7", "8", "9","p","o","i","n","t","s"]);
		protected var letters:Dictionary;
		protected var textureManager:TextureManager;
		protected var style:String;
		public function Font(textureManager:TextureManager, style:String) 
		{
			this.textureManager = textureManager;
			letters = new Dictionary();
			this.style = style;
			
			offsets = new Dictionary();
			loadJSON();
		}
		
		private function loadJSON():void 
		{
			var loader:URLLoader;
			loader = new URLLoader(new URLRequest("json/" + style + ".json"));
			loader.addEventListener(Event.COMPLETE, onComplete);
		}
		private function onComplete(e:Event):void 
		{
			e.target.removeEventListener(Event.COMPLETE, onComplete);
			var data:String = e.target.data;
			data = data.replace("\r\n", "");
			var offsetsArray:Array = JSON.parse(data).frames;
			for each(var offset:Object in offsetsArray)
			{
				offsets[parseInt(offset.name)] = parseInt(offset.position.split(",")[1]);
			}
			loaded = true;
			dispatchEvent(new Event(Event.COMPLETE));
		}
		public function getLetter(char:String):Bitmap3D
		{
			if (letters[char] == undefined)
				addLetter(char)
			var result:Bitmap3D = new Bitmap3D(letters[char]);
			result.offsetY = offsets[char.charCodeAt()];
			return result;
		}
		private function addLetter(char:String):void
		{
			var letter:Texture3D = textureManager.getTexture3D(style + "_" + char.charCodeAt());
			if (letter)
			{
				letters[char] = letter;
			}
		}
	}

}