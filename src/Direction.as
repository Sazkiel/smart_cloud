package  
{
	/**
	 * ...
	 * @author game
	 */
	public class Direction 
	{
		public static const All:int = 0;
		public static const Top:int = 1;
		public static const Bottom:int = 2;
		public static const Right:int = 3;
		public static const Left:int = 4;
	}

}