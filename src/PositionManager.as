package  
{
	import display.DisplayObject3D;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import tween.Tweener;
	/**
	 * ...
	 * @author mawo
	 */
	public class PositionManager extends EventDispatcher
	{
		private var boardAsset:DisplayObject3D;
		private var engine:Engine;
		private var currentObject:BoardObject;
		public function PositionManager(boardAsset:DisplayObject3D, engine:Engine) 
		{
			this.boardAsset = boardAsset;
			this.engine = engine;
			boardAsset.mouseEnabled = true;
			enable = true;
		}
		private function onObjectMouseUp(e:MouseEvent):void 
		{
			engine.userInputManager.removeEventListener(MouseEvent.MOUSE_MOVE, onMove);
			currentObject.mouseEnabled = true;
		}
		
		private function onObjectMouseDown(e:MouseEvent):void 
		{
			currentObject.mouseEnabled = false;
			engine.userInputManager.addEventListener(MouseEvent.MOUSE_MOVE, onMove);
			onMove(e);
			//Tweener.move(currentObject, getOnBoardX(e), getOnBoardY(e), 200);
		}
		private function putObjectOnBoard(e:MouseEvent):void
		{
			dispatchEvent(new Event(EventType.PUT_ON_BOARD));
		}
		private function onMove(e:MouseEvent):void 
		{
			if (e.localX > boardAsset.x && e.localX < (boardAsset.x + boardAsset.width) && e.localY > boardAsset.y && e.localY < (boardAsset.y + boardAsset.height))
			{
				currentObject.x = getOnBoardX(Math.floor((e.localX - boardAsset.x) / Config.CellSize), currentObject.width);
				currentObject.y = getOnBoardY(Math.floor(((e.localY - boardAsset.y)  / Config.CellSize)),currentObject.height);
			}
			else
			{
				currentObject.x = e.localX - currentObject.width / 2;
				currentObject.y = e.localY - currentObject.height / 2;
			}			
		}
		private function getOnBoardX(coll:int, objectWidth:Number):Number
		{
			return coll * Config.CellSize + boardAsset.x + (Config.CellSize - objectWidth) / 2;
		}		
		private function getOnBoardY(row:int, objectHeight:Number):Number
		{
			return row * Config.CellSize + boardAsset.y + (Config.CellSize - objectHeight) / 2;
		}
		
		public function setCurrentObject(value:BoardObject):void 
		{
			if (currentObject)
			{
				currentObject.removeEventListener(MouseEvent.MOUSE_DOWN, onObjectMouseDown);
				currentObject = null;
			}
			if (value)
			{
				currentObject = value;
				currentObject.x = 10;
				currentObject.y = 100;
				currentObject.addEventListener(MouseEvent.MOUSE_DOWN, onObjectMouseDown);
				engine.userInputManager.addEventListener(MouseEvent.MOUSE_UP, onObjectMouseUp);
			}
		}
		
		public function resetPosition():void 
		{
			if(currentObject)
				Tweener.move(currentObject, 10, 100, 200);
		}
		
		public function setPosition(mergeResult:BoardObject):void 
		{
			mergeResult.x = getOnBoardX(mergeResult.coll, mergeResult.width);
			mergeResult.y = getOnBoardY(mergeResult.row, mergeResult.height);
		}
		
		public function set enable(value:Boolean):void 
		{
			if (value)
			{
				boardAsset.addEventListener(MouseEvent.MOUSE_DOWN, onObjectMouseDown);
				boardAsset.addEventListener(MouseEvent.MOUSE_UP, putObjectOnBoard);
			}
			else
			{
				boardAsset.removeEventListener(MouseEvent.MOUSE_DOWN, onObjectMouseDown);
				boardAsset.removeEventListener(MouseEvent.MOUSE_UP, putObjectOnBoard);
			}
		}
	}

}