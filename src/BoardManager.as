package  
{
	import display.Bitmap3D;
	import display.TextField3D;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import tween.Tweener;
	/**
	 * ...
	 * @author mawo
	 */
	public class BoardManager 
	{
		private var currentObject:BoardObject;
		private var boardObjects:Vector.<Vector.<BoardObject>>;
		private var boardAsset:Bitmap3D;
		private var text:TextField3D;
		private var engine:Engine;
		private var context:Context;
		private var positionManager:PositionManager;
		private var mergeManager:MergeManager;
		
		public function BoardManager(engine:Engine, context:Context) 
		{
			this.engine = engine;
			this.context = context;
			
			boardObjects = new Vector.<Vector.<BoardObject>>();
			for (var i:int; i < 6; i++)
			{
				boardObjects.push(new Vector.<BoardObject>(6));
			}
			
			addBoardAsset();
			positionManager = new PositionManager(boardAsset, engine);
			positionManager.addEventListener(EventType.PUT_ON_BOARD, putOnBoard);
			
			mergeManager = new MergeManager(boardObjects);
			mergeManager.addEventListener(EventType.MERGE, onMerge);
			addRandomBoardObject()
		}
		
		private function onMerge(e:MergeEvent):void 
		{
			for (var i:int; i < e.infos.length; i++)
			{
				boardObjects[e.infos[i].row][e.infos[i].coll].parent.removeChild(boardObjects[e.infos[i].row][e.infos[i].coll]);
				boardObjects[e.infos[i].row][e.infos[i].coll] = null;
			}
			if(addMergeResult(e.object) && mergeManager.merge(e.object.row, e.object.coll) == false)
				positionManager.enable = true;
			context.dispatchEvent(e);
			//var newObject:BoardObject = addNewObject(object.info.mergeId, object.x, object.y);
			//newObject.mouseEnabled = false;
			//putOnBoard(newObject);
		}
		
		private function addMergeResult(object:BoardObject):Boolean 
		{
			var info:BoardObjectInfo = context.content.getBoardObjectInfo(object.info.mergeId);
			var mergeResult:BoardObject = new BoardObject(info, engine);
			mergeResult.row = object.row;
			mergeResult.coll = object.coll;
			boardObjects[object.row][object.coll] = mergeResult;
			setObjectScale(mergeResult);
			positionManager.setPosition(mergeResult);
			mergeResult.mouseEnabled = false;
			addBoardObject(mergeResult);
			return true;
		}
		
		private function putOnBoard(e:Event):void 
		{
			var coll:int = (engine.stage.mouseX - boardAsset.x) / Config.CellSize;
			var row:int = (engine.stage.mouseY - boardAsset.y) / Config.CellSize;
			if (boardObjects[row][coll] == null)
			{
				currentObject.mouseEnabled = false;
				boardObjects[row][coll] = currentObject;
				currentObject.row = row;
				currentObject.coll = coll;
				if (mergeManager.merge(row, coll))
					positionManager.enable = false;
				positionManager.setCurrentObject(null);
				addRandomBoardObject();
			}
			else
			{
				positionManager.resetPosition();
			}
		}
		public function addBoardObject(object:BoardObject):void
		{
			engine.layerManager.addToLayer(object, LayerType.BOARD);
		}

		private function setObjectScale(object:BoardObject):void
		{
			if (object.width > object.height)
			{
				object.scaleX = Config.CellSize / object.width;
				object.height = object.scaleX * object.height;
			}
			else
			{
				object.scaleY = Config.CellSize / object.height;
				object.width = object.scaleY * object.width;
			}
			object.scaleX *= 0.9;
			object.scaleY *= 0.9;
			
		}
		private function addRandomBoardObject():void 
		{
			var info:BoardObjectInfo = context.content.boardObjects[0];
			currentObject = new BoardObject(info, engine);
			setObjectScale(currentObject);
			positionManager.setCurrentObject(currentObject);
			addBoardObject(currentObject);
		}
		
		private function addBoardAsset():void 
		{
			boardAsset = engine.assetManager.getBitmap3D("board");
			engine.layerManager.addToLayer(boardAsset, LayerType.BOARD);
			boardAsset.width = engine.stageWidth - 20;
			boardAsset.height = engine.stageWidth - 20;
			boardAsset.x = 10;
			boardAsset.y = 180;
			Config.CellSize = boardAsset.width / Config.BoardSize;
		}
	}
}