package  
{
	import display.Bitmap3D;
	import tween.Tweener;
	/**
	 * ...
	 * @author mawo
	 */
	public class BackgroundManager
	{
		private var engine:Engine;
		private var background:Bitmap3D;
		private var balloon1:Bitmap3D;
		private var balloon2:Bitmap3D;
		public function BackgroundManager(engine:Engine) 
		{
			this.engine = engine;
			background = engine.assetManager.getBitmap3D("background");
			background.width = engine.stageWidth;
			background.height = engine.stageHeight;
			//background.blendType = BlendType.SCREEN;
			engine.layerManager.addToLayer(background, LayerType.BACKGROUND);
			
			balloon1 = engine.assetManager.getBitmap3D("balloon1");
			balloon2 = engine.assetManager.getBitmap3D("balloon2");
			
			engine.layerManager.addToLayer(balloon1, LayerType.BACKGROUND);
			engine.layerManager.addToLayer(balloon2, LayerType.BACKGROUND);
			tweenBallon1();
			tweenBallon2();
		}
		
		private function tweenBallon1():void 
		{
			balloon1.scaleX = balloon1.scaleY = 0.5;
			balloon1.x = -balloon1.width;
			balloon1.y = Math.random() * 400 + 100;
			Tweener.move(balloon1, engine.stageWidth, Math.random() * 400, 15000)
			Tweener.call(tweenBallon1, 20000)
		}		
		
		private function tweenBallon2():void 
		{
			balloon2.scaleX = balloon2.scaleY = 0.5;
			balloon2.x = -balloon2.width;
			balloon2.y = Math.random() * 400 + 100;
			Tweener.move(balloon2, engine.stageWidth, Math.random() * 400, 20000)
			Tweener.call(tweenBallon2, 25000)
		}
		
	}

}