package 
{
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	
	/**
	 * ...
	 * @author mawo
	 */
	public class Main extends Sprite 
	{
		private var engine:Engine;
		private var background:BackgroundManager;
		private var board:BoardManager;
		private var gui:GuiManager;
		private var content:Content;
		private var context:Context;
		public function Main():void 
		{
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			// touch or gesture?
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			
			engine = new Engine(stage);
			engine.loadTextures(Vector.<String>(["background", "board", "fonts", "board_objects"]));
			engine.loadAnimations(Vector.<String>(["board_objects"]));
			engine.loadFonts(Vector.<String>([FontType.GLACIAL_PURPLE, FontType.GLACIAL_YELLOW]));
			engine.createLayers(LayerType.LAYER_COUNT);
			engine.addEventListener(EngineEvents.ENGINE_CREATED, onEngineCreate);
		}
		
		private function onEngineCreate(e:Event):void 
		{
			context = new Context();
			background = new BackgroundManager(engine);
			board = new BoardManager(engine, context);
			gui = new GuiManager(engine, context);
			//var obj:Object = JSON.parse('{"animation":"two_clouds_sun", "children":[{"texture":"sun", "keyframe":0, "x":19.35, "y":20.55},{"texture":"cloud1", "keyframe":0, "x":11.75, "y":77.25},{"texture":"cloud1", "keyframe":79, "x":17, "y":73.9},{"texture":"cloud1", "keyframe":159, "x":17.75, "y":81.75},{"texture":"cloud1", "keyframe":239, "x":11.75, "y":77.25},{"texture":"cloud2", "keyframe":0, "x":74.3, "y":116.8},{"texture":"cloud2", "keyframe":79, "x":66.95, "y":123},{"texture":"cloud2", "keyframe":159, "x":74.3, "y":105.8},{"texture":"cloud2", "keyframe":239, "x":74.3, "y":116.8}]}');
		}
		
	}
	
}