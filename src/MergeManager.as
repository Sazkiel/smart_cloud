package  
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import tween.Tweener;
	/**
	 * ...
	 * @author mawo
	 */
	public class MergeManager extends EventDispatcher
	{
		private var boardObjects:Vector.<Vector.<BoardObject>>;
		public function MergeManager(boardObjects:Vector.<Vector.<BoardObject>>) 
		{
			this.boardObjects = boardObjects;
		}
		public function merge(row:int, coll:int):Boolean 
		{
			var infos:Vector.<MergeInfo> = getMergeInfo(row, coll, Direction.All, boardObjects[row][coll].info.id);
			infos.push(new MergeInfo(row, coll));
			if (infos.length >= Config.MergeLimit)
			{
				for (var i:int; i < infos.length; i++)
				{
					Tweener.move(boardObjects[infos[i].row][infos[i].coll], boardObjects[row][coll].x, boardObjects[row][coll].y, 250);
				}
				Tweener.call(afterMerge, 260, [infos, boardObjects[row][coll]]);
				return true;
			}
			return false;
		}
		
		private function afterMerge(infos:Vector.<MergeInfo>, object:BoardObject):void 
		{
			dispatchEvent(new MergeEvent(infos, object));
			//G.player.addPoints(infos.length * object.info.mergePoints);
			//G.player.addCurrency(object.info.rewardCurrency);
			//Fx.addCurrency(object.info.rewardCurrency, object.x, object.y, G.hud.wallet.x, G.hud.wallet.y);
			//for (var i:int; i < infos.length; i++)
			//{
				//boardObjects[infos[i].row][infos[i].coll].dispose();
				//boardObjects[infos[i].row][infos[i].coll] = null;
			//}
			//var newObject:BoardObject = addNewObject(object.info.mergeId, object.x, object.y);
			//newObject.mouseEnabled = false;
			//putOnBoard(newObject);
		}
		private function getMergeInfo(row:int, coll:int, from:int, compareId:int):Vector.<MergeInfo>
		{
			var result:Vector.<MergeInfo> = new Vector.<MergeInfo>();
			if(from != Direction.Bottom && checkNeighbor(row + 1, coll, row, coll, compareId, result))
				result = result.concat(getMergeInfo(row + 1, coll, Direction.Top, compareId));
				
			if (from != Direction.Top && checkNeighbor(row - 1, coll, row, coll, compareId, result))
				result = result.concat(getMergeInfo(row - 1, coll, Direction.Bottom, compareId));
				
			if(from != Direction.Left && checkNeighbor(row, coll - 1, row, coll, compareId, result))
				result = result.concat(getMergeInfo(row, coll - 1, Direction.Right, compareId));
				
			if(from != Direction.Right && checkNeighbor(row, coll + 1, row, coll, compareId, result))
				result = result.concat(getMergeInfo(row, coll + 1, Direction.Left, compareId));
				
			return result;
		}
		private function checkNeighbor(row:int, coll:int, parentRow:int, parentColl:int, compareId:int, result:Vector.<MergeInfo>):Boolean
		{
			if (row < 0 || coll < 0 || row >= Config.BoardSize || coll >= Config.BoardSize)
				return false;
			if (boardObjects[row][coll] && boardObjects[row][coll].info.id == compareId)
			{
				result.push(new MergeInfo(row, coll));
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
