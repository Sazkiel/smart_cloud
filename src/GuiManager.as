package  
{
	import display.Bitmap3D;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import tween.MoveType;
	import tween.Tweener;
	/**
	 * ...
	 * @author mawo
	 */
	public class GuiManager
	{
		private var points:TeaseField;
		private var gold:TeaseField;
		
		private var engine:Engine;
		private var context:Context;
		private var coin:Bitmap3D;
		public function GuiManager(engine:Engine, context:Context) 
		{
			this.engine = engine;
			this.context = context;
			context.addEventListener(EventType.MERGE, onMerge);
			points = new TeaseField(engine, FontType.GLACIAL_PURPLE);
			points.value = 0;
			points.x = points.y = 10;
			engine.layerManager.addToLayer(points.field, LayerType.GUI);
			
			gold = new TeaseField(engine, FontType.GLACIAL_YELLOW);
			gold.value = 0;
			gold.x = engine.stageWidth - gold.width - 10;
			gold.y = 10;
			engine.layerManager.addToLayer(gold.field, LayerType.GUI);
		}
		
		private function onMerge(e:MergeEvent):void 
		{
			points.value += e.points;
			gold.value += e.gold;
			gold.x = engine.stageWidth - gold.width - 10;
			coin = engine.assetManager.getBitmap3D("coin");
			coin.x = e.object.x;
			coin.y = e.object.y;
			coin.scaleX = coin.scaleY = 0.2;
			engine.layerManager.addToLayer(coin, LayerType.GUI);
			Tweener.move(coin, gold.x, gold.y, 2, MoveType.BEZIER);
		}
	}

}