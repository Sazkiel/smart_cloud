package  
{
	import flash.events.Event;
	/**
	 * ...
	 * @author mawo
	 */
	public class MergeEvent extends Event
	{
		public var object:BoardObject;
		public var infos:Vector.<MergeInfo>;
		
		public function MergeEvent(infos:Vector.<MergeInfo>, object:BoardObject) 
		{
			super(EventType.MERGE);
			this.infos = infos;
			this.object = object;
		}
		public function get points():int
		{
			return object.info.points * infos.length;
		}		
		public function get gold():int
		{
			return object.info.gold * infos.length;
		}
		override public function clone():Event
		{
			return new MergeEvent(infos, object);
		}
	}

}