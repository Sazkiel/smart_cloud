package  
{
	/**
	 * ...
	 * @author mawo
	 */
	public class BoardObjectInfo
	{
		public var id:int;
		public var asset:String;
		public var mergeId:int
		public var points:int;
		public var gold:int;
		public function BoardObjectInfo(id:int, mergeId:int, asset:String, points:int, gold:int) 
		{
			this.id = id;
			this.mergeId = mergeId;
			this.asset = asset;
			this.points = points;
			this.gold = gold;
		}
		
	}

}