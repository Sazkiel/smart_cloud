package  
{
	import display.DisplayObject3D;
	import display.DisplayObjectContainer3D;
	/**
	 * ...
	 * @author mawo
	 */
	public class BoardObject extends DisplayObjectContainer3D
	{
		public var info:BoardObjectInfo;
		public var asset:DisplayObject3D;
		public var row:int;
		public var coll:int;
		
		public function BoardObject(info:BoardObjectInfo, engine:Engine) 
		{
			mouseEnabled = true;
			this.info = info;
			asset = engine.assetManager.getMovieClip3D(info.asset);
			asset.mouseEnabled = true;
			addChild(asset);
		}
		public function merge(object:BoardObject):Boolean
		{
			return object.info.id == info.id;
		}
	}

}