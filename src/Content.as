package  
{
	/**
	 * ...
	 * @author mawo
	 */
	public class Content 
	{
		public var boardObjects:Vector.<BoardObjectInfo>;
		public function Content() 
		{
			addBoardObjects()
		}
		
		private function addBoardObjects():void 
		{
			boardObjects = new Vector.<BoardObjectInfo>();
			boardObjects.push(new BoardObjectInfo(0, 1, "two_clouds_sun", 10, 7));
			boardObjects.push(new BoardObjectInfo(1, 2, "two_clouds", 30, 20));
			boardObjects.push(new BoardObjectInfo(2, 3, "rain", 50, 40));
			boardObjects.push(new BoardObjectInfo(3, 4, "storm", 80, 50));
			boardObjects.push(new BoardObjectInfo(4, 5, "rainbow", 100, 70));
			boardObjects.push(new BoardObjectInfo(5, 0, "sun", 150, 130));
		}
		
		public function getBoardObjectInfo(id:int):BoardObjectInfo
		{
			return boardObjects[id];
		}
	}

}