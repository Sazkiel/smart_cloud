package  
{
	import display.TextField3D;
	/**
	 * ...
	 * @author mawo
	 */
	public class TeaseField
	{
		private var m_value:Number;
		public var field:TextField3D;
		public function TeaseField(engine:Engine, style:String) 
		{
			this.field = engine.fontManager.getTextField3D(style);
			field.scaleX = field.scaleY;
			m_value = 0;
		}
		public function set value(value:Number):void
		{
			m_value = value;
			field.text = m_value.toString();
		}
		public function get value():Number
		{
			return m_value;
		}
		public function addText(value:String):void
		{
			field.text += value;
		}
		public function get width():Number
		{
			return field.width;
		}
		public function get height():Number
		{
			return field.height;
		}		
		public function set x(value:Number):void
		{
			field.x = value;
		}
		public function set y(value:Number):void
		{
			field.y = value;
		}		
		public function get x():Number
		{
			return field.x;
		}
		public function get y():Number
		{
			return field.y;
		}
	}

}