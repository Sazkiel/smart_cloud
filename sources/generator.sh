#!/bin/sh

TexturePacker background/*.png --data background.json --format json-array --sheet background.png --max-width 1024 --max-height 1024
TexturePacker board/*.png --data board.json --format json-array --sheet board.png --max-width 1024 --max-height 1024
TexturePacker board_objects/*.png --data board_objects.json --format json-array --sheet board_objects.png --max-width 1024 --max-height 1024
TexturePacker fonts/*.png --data fonts.json --format json-array --sheet fonts.png --max-width 1024 --max-height 1024

for f in *.png; do png2atf -c [d,e,p] -n 0,0 -i $f -o ${f%.*}.atf ; done
rm ../bin/atf/*.atf
cp *.atf ../bin/atf

rm ../bin/json/*.json 
cp *.json ../bin/json
cp fonts/*.json ../bin/json
rm *.atf
rm *.json
#rm *.png
