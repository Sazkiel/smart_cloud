﻿var fileURL = fl.browseForFileURL("select", "Select file");
fl.outputPanel.clear();
var doc = fl.openDocument(fileURL);
if (doc)
{
	doc = fl.getDocumentDOM();
	var json = "{\"animations\":[";
	for( var i = 0; i < doc.library.items.length; i++){
		var item = doc.library.items[i];
		if(item.linkageExportForAS)
		{
			json += "{\"name\":\"" + item.name + "\", \"timelines\":[";
			for(var layer_index = item.timeline.layerCount - 1; layer_index > -1; layer_index--)
			{
				var layer = item.timeline.layers[layer_index];
				var frames = layer.frames;
				json += "{\"name\":\"" + layer.name + "\",\"keyframes\":[";
				for(var frame_index = 0; frame_index < layer.frameCount; frame_index++)
				{
					
					if(frame_index == frames[frame_index].startFrame)
					{
						var child = frames[frame_index].elements[0];
						json += "{\"index\":"+frame_index + ", \"x\":" + child.x + ", \"y\":" + child.y + ", \"scaleX\":" + child.scaleX + ", \"scaleY\":" + child.scaleY + ", \"alpha\":" + child.colorAlphaPercent +"},";
					}
					
				}
				json = json.slice(0,json.length - 1);
				json += "]},";
			}
			json = json.slice(0,json.length - 1);
			json += "]},";
		}
	}
	json = json.slice(0,json.length - 1);
	json += "]}";
	fl.trace(json);
}